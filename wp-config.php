<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'qtest-db' );

/** MySQL database username */
define( 'DB_USER', 'qtestdb-usr' );

/** MySQL database password */
define( 'DB_PASSWORD', 'SLquincie1234' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ffR]XI9_.]Mnm8e.Q=WLRo=#;me-LKRX&E!O}ToENhSgMc_)`&}BNjWzpG>TgLf|' );
define( 'SECURE_AUTH_KEY',  '<:!Wd<-s.kUA7Pmi ?Dk[4phEUNSZVzfhD#mpl*0zbUD&]eW5cry&J1Q6Gg%v[@D' );
define( 'LOGGED_IN_KEY',    'rPtV@r4lVrZ$?5Lk~[c1Rv$e_4#yxtd =/lK7{DM9&{.|`{HVhU927@u]*Oxwey$' );
define( 'NONCE_KEY',        'GkFB8LAgc-xz-e$v+Z!f=IFIhN|ijE@Cg++s9ihp,>[Xl5eX3 TZr=B}6{ByW/$S' );
define( 'AUTH_SALT',        '9oK*(=-$X%mGwkU#@?LL3HY0{|F,Q+o{}oucd,OANK5o]qlZp@r}tj#yfY]eNGY_' );
define( 'SECURE_AUTH_SALT', 'j=n[YbM+d>zXO{:?AhF;jR2V/W4!-~o|QNw d}0PIe_Kryi._N?W`0*fk_e3y=hR' );
define( 'LOGGED_IN_SALT',   'I|HLLo!tV<Lv!$71Bx[HVi>n`RJU.p-A^u!CVhvcra(-JCM8)!2h}!X#UBV&{lV;' );
define( 'NONCE_SALT',       '(tw@Aa(nr<!4eNc6I:E{QsY&Upj6LHd_rFEMItqD*@(@@=>CN0Iy4JV~~6-KedkP' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
